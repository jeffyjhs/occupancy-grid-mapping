 function [mapUpdate, robPoseMapFrame, laserEndPntsMapFrame] = inv_sensor_model(map, scan, robPose, gridSize, offset, probPrior, probOcc, probFree)
% Compute the log odds values that should be added to the map based on the inverse sensor model
% of a laser range finder.

% map is the matrix containing the occupancy values (IN LOG ODDS) of each cell in the map.
% scan is a laser scan made at this time step. Contains the range readings of each laser beam.
% robPose is the robot pose in the world coordinate frame.
% gridSize is the size of each grid in meters.
% offset = [offsetX; offsetY] is the offset that needs to be subtracted from a point
% when converting to map coordinates.
% probOcc is the probability that a cell is occupied by an obstacle given that a
% laser beam endpoint hit that cell.
% probFree is the probability that a cell is occupied given that a laser beam passed through it.

% mapUpdate is a matrix of the same size as map. It has the log odds values that need to be added for the cells
% affected by the current laser scan.
% robPoseMapFrame is the pose of the robot in the map coordinate frame.
% laserEndPntsMapFrame are map coordinates of the endpoints of each laser beam (also used for visualization purposes).

% TODO: Initialize mapUpdate.
mapUpdate = zeros(size(map));
% Robot pose as a homogeneous transformation matrix.
robTrans = v2t(robPose);

% TODO: compute robPoseMapFrame. Use your world_to_map_coordinates implementation.
robPoseMapFrame = world_to_map_coordinates(robPose, gridSize, offset);
% Compute the Cartesian coordinates of the laser beam endpoints.
% Set the third argument to 'true' to use only half the beams for speeding up the algorithm when debugging.
laserEndPnts = robotlaser_as_cartesian(scan, 30, false);

% Compute the endpoints of the laser beams in the world coordinates frame.
laserEndPnts = robTrans*laserEndPnts;
% TODO: compute laserEndPntsMapFrame from laserEndPnts. Use your world_to_map_coordinates implementation.
laserEndPntsMapFrame = world_to_map_coordinates(laserEndPnts, gridSize, offset);
% Iterate over each laser beam and compute freeCells.
% Use the bresenham method available to you in tools for computing the X and Y
% coordinates of the points that lie on a line.
% Example use for a line between points p1 and p2:
% [X,Y] = bresenham([p1_x, p1_y; p2_x, p2_y]);
% You only need the X and Y outputs of this function.
occpCells = [];
for sc=1:columns(laserEndPntsMapFrame)
    %TODO: compute the XY map coordinates of the free cells along the laser beam ending in laserEndPntsMapFrame(:,sc)
    [X,Y] = bresenham([robPoseMapFrame(1) ,robPoseMapFrame(2); laserEndPntsMapFrame(1,sc),laserEndPntsMapFrame(2,sc)]);
    cor = [X;Y];
    len = size(cor,2);
    head = norm(cor(:,1)-robPoseMapFrame);
    tail = norm(cor(:,len)-robPoseMapFrame);
    if tail < head
        cor = fliplr(cor);
    end
    %TODO: add them to freeCells and occpCells
    % freeCells are the map coordinates of the cells through which the laser beams pass.
    % occpCells are the map coordinates of the cells through which the laser beams end.
    if len >1
            freeCells = cor(:,1:len-1);
    end
    occpCells = [occpCells,cor(:,len)];
    %TODO: update the log odds values in mapUpdate for each free cell according to probFree.
    if len > 1
        for i = 1:size(freeCells,2)
            mapUpdate(freeCells(1,i),freeCells(2,i)) = mapUpdate(freeCells(1,i),freeCells(2,i)) + prob_to_log_odds(probFree);
%             mapUpdate(freeCells(1,i),freeCells(2,i)) = prob_to_log_odds(probFree);
        end
    end
    %TODO: update the log odds values in mapUpdate for each laser endpoint according to probOcc.
%     mapUpdate(occpCells(1),occpCells(2)) = mapUpdate(occpCells(1),occpCells(2)) + prob_to_log_odds(probOcc);
%     mapUpdate(occpCells(1),occpCells(2)) = prob_to_log_odds(probOcc);
end
for i = 1:size(occpCells,2)
    if mapUpdate(occpCells(1,i),occpCells(2,i))< 0 
        mapUpdate(occpCells(1,i),occpCells(2,i)) = prob_to_log_odds(probOcc);
    else
        mapUpdate(occpCells(1,i),occpCells(2,i)) = mapUpdate(occpCells(1,i),occpCells(2,i)) + prob_to_log_odds(probOcc);
    end
end
end

